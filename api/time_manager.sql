/*
Navicat MySQL Data Transfer

Source Server         : 0localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : time_manager

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-11-21 03:32:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0' COMMENT '用户ID',
  `record_date` date DEFAULT NULL COMMENT '记录日期',
  `record_begin` varchar(255) DEFAULT '' COMMENT '记录开始时间',
  `record_end` varchar(255) DEFAULT '' COMMENT '记录结束时间',
  `record_long` int(11) DEFAULT '0' COMMENT '记录时长，分钟',
  `type_id` int(11) DEFAULT '0' COMMENT '记录类型',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `ext1` text COMMENT '扩展字段1',
  `ext2` text COMMENT '扩展字段2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='时间记录';

-- ----------------------------
-- Records of record
-- ----------------------------
INSERT INTO `record` VALUES ('4', '7', '2021-11-21', '01:45', '02:45', '60', '1', '55', '2021-11-21 02:45:57', '2021-11-21 02:45:57', null, null);
INSERT INTO `record` VALUES ('5', '7', '2021-11-21', '01:47', '02:47', '60', '1', '333', '2021-11-21 02:47:54', '2021-11-21 02:47:54', null, null);
INSERT INTO `record` VALUES ('6', '7', '2021-11-21', '01:48', '02:48', '60', '1', 'eee', '2021-11-21 02:48:25', '2021-11-21 02:48:25', null, null);
INSERT INTO `record` VALUES ('8', '7', '2021-11-21', '01:53', '02:54', '61', '2', 'ddd', '2021-11-21 02:53:48', '2021-11-21 02:53:48', null, null);
INSERT INTO `record` VALUES ('9', '7', '2021-11-22', '01:54', '02:54', '60', '1', 'www', '2021-11-21 02:54:51', '2021-11-21 02:54:51', null, null);
INSERT INTO `record` VALUES ('10', '7', '2021-11-21', '01:43', '02:43', '60', '2', '5666', '2021-11-21 03:02:34', '2021-11-21 03:02:34', null, null);
INSERT INTO `record` VALUES ('16', '7', '2021-11-21', '01:36', '02:43', '67', '4', '5666', '2021-11-21 03:10:31', '2021-11-21 03:10:31', null, null);
INSERT INTO `record` VALUES ('18', '7', '2021-11-21', '01:45', '04:45', '180', '1', '55', '2021-11-21 03:19:00', '2021-11-21 03:19:00', null, null);
INSERT INTO `record` VALUES ('19', '7', '2021-11-21', '00:32', '02:43', '131', '1', '我爱中国', '2021-11-21 03:20:09', '2021-11-21 03:21:39', null, null);
INSERT INTO `record` VALUES ('20', '7', '2021-11-21', '00:36', '02:43', '127', '3', '我中国', '2021-11-21 03:20:14', '2021-11-21 03:21:48', null, null);
INSERT INTO `record` VALUES ('21', '7', '2021-11-24', '02:31', '03:31', '60', '1', '我爱学习', '2021-11-21 03:31:50', '2021-11-21 03:31:50', null, null);

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '' COMMENT '类型名称',
  `color` varchar(255) DEFAULT '' COMMENT '颜色',
  `valid` int(11) DEFAULT '1' COMMENT '是否有效，0无效，1有效',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `ext1` text COMMENT '扩展字段1',
  `ext2` text COMMENT '扩展字段2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='类型表';

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES ('1', '学习', '#2979ff', '1', null, null, null, null);
INSERT INTO `type` VALUES ('2', '游戏', '#ff9900', '1', null, null, null, null);
INSERT INTO `type` VALUES ('3', '运动', '#19be6b', '1', null, null, null, null);
INSERT INTO `type` VALUES ('4', '恋爱', ' #fa3534', '1', null, null, null, null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `login_name` varchar(255) DEFAULT NULL COMMENT '管理员账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `salt` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '密码盐，用于和密码明文组合加密',
  `valid` tinyint(11) DEFAULT '1' COMMENT '是否有效，0：无效，1：有效',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `ext1` text COMMENT '扩展字段1',
  `ext2` text COMMENT '扩展字段2',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `login_count` int(11) DEFAULT '0' COMMENT '登录次数',
  `token` varchar(255) DEFAULT '' COMMENT 'token',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('7', '123', 'e3497c5e785263d924514bf6282804b0', '9215', '1', '2021-11-21 02:06:12', '2021-11-21 03:31:20', '9215123456', null, '2021-11-21 03:31:20', '7', 'YTZlNjU3ODMyMGM5MDM5ZDlkYTQ2YTQ3OWNiYWVlYTB8N3wxNjM4MDQxNDgw');
