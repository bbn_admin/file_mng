<?php
// 应用公共文件




if (!function_exists('returnFormat')) {
    /**
     * 格式化输出返回值
     * @param int $code
     * @param string $msg
     * @param array $data
     * @return mixed
     */
    function returnFormat($code=0,$msg="",$data=[]){
        $res['code']=$code;
        $res['data']=$data;
        $res['msg']=$msg;
        return $res;
    }
}





if (!function_exists('each_item')) {
    function each_item(&$array)
    {
        $res = array();
        $key = key($array);
        if ($key !== null) {
            next($array);
            $res[1] = $res['value'] = $array[$key];
            $res[0] = $res['key'] = $key;
        } else {
            $res = false;
        }
        return $res;
    }
}

if (!function_exists('getVirRootDir')) {
    /**
     * 获取虚拟目录路径
     * @return bool|string
     */
    function getVirRootDir()
    {
        $url = $_SERVER['SCRIPT_NAME'];
        $url = substr($url, 0, strripos($url, "/"));
        return $url;
    }
}


if (!function_exists('getNow')) {
    /**
     * 获取当时时间
     * @param string $fmt 格式化
     * @return false|string
     */
    function getNow($fmt = "Y-m-d H:i:s")
    {
        return date($fmt);
    }
}

if (!function_exists('getUrl')) {
    /**
     * 获取当前的访问路径
     * @return [type] [description]
     */
    function getUrl()
    {
        $sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
        $php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
        $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
        $relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self . (isset($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : $path_info);
        return $sys_protocal . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . $relate_url;
    }
}

if(!function_exists('number2chinese')){

    function number2chinese($num)
    {
        $arr = array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');
        $cny = array('', '拾', '佰', '仟', '', '萬', '亿', '兆');
        //小数部分
        $retval = '';
        if (strpos($num, '.') !== false) {
            list($num, $dec) = explode('.', $num);
            $retval .= $arr[$dec[0]] . '角' . $arr[$dec[1]] . '分';
        }
        //整数部分
        $str = $num != '0' ? strrev($num) : '';
        $out = array();
        for ($i = 0; $i < strlen($str); $i++) {
            $out[$i] = $arr[$str[$i]];
            $out[$i] .= $str[$i] != '0' ? $cny[$i % 4] : '';
            if ($i > 1 && $str[$i] + $str[$i - 1] == 0) {
                $out[$i] = '';
            }
            if ($i % 4 == 0) {
                $out[$i] .= $cny[4 + floor($i / 4)];
            }
            //echo $out[$i].'<br>';
        }
        $retval = implode('', array_reverse($out)) . '元' . $retval;
        return $retval;
    }
}


if (!function_exists('toUnderScore')) {
    /**
     * 驼峰命名转下划线命名
     * 思路:
     * 小写和大写紧挨一起的地方,加上分隔符,然后全部转小写
     * @param $camelCaps
     * @param string $separator
     * @return string
     * @author web
     */
    function toUnderScore($camelCaps, $separator = '_')
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
    }
}


if (!function_exists('convertUTF8')) {
    /**
     * 解决中文乱码的问题
     * @param $str
     * @return string
     */
    function convertUTF8($str)
    {
        if (empty($str)) return '';
        return iconv('utf-8', 'gb2312', $str);
    }
}
if (!function_exists('isIdCardNo')) {
    /**
     * 判断是否为合法的身份证号码
     * @param $mobile
     * @return int
     */
    function isIdCardNo($vStr)
    {
        $vCity = array(
            '11', '12', '13', '14', '15', '21', '22',
            '23', '31', '32', '33', '34', '35', '36',
            '37', '41', '42', '43', '44', '45', '46',
            '50', '51', '52', '53', '54', '61', '62',
            '63', '64', '65', '71', '81', '82', '91'
        );

        if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) return false;
        if (!in_array(substr($vStr, 0, 2), $vCity)) return false;
        $vStr = preg_replace('/[xX]$/i', 'a', $vStr);
        $vLength = strlen($vStr);
        if ($vLength == 18) {
            $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
        } else {
            $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
            return false;//不考虑一代身份证了
        }
        if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) return false;
        if ($vLength == 18) {
            $vSum = 0;
            for ($i = 17; $i >= 0; $i--) {
                $vSubStr = substr($vStr, 17 - $i, 1);
                $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr, 11));
            }
            if ($vSum % 11 != 1) return false;
        }
        return true;
    }
}
if (!function_exists('cleanEnter')) {
    /**
     * 清除回车换行和前后空格
     * @param $str
     * @return array|string|string[]
     */
    function cleanEnter($str)
    {
        $str = trim($str);
        $str = str_replace("\n", "", $str);
        $str = str_replace("\r", "", $str);
        return $str;
    }
}

if (!function_exists('randNum')) {
    /**
     * 获取数字随机数
     * @param $length 数字长度
     * @return int
     */
    function randNum($length = 8)
    {
        $min = pow(10, $length-1) + 1;
        $max = pow(10, $length ) - 1;
        $rand = rand($min, $max);
//        echo "length: $length, min: $min ,max: $max ,rand: $rand \r\n <br/>";
        return $rand;
    }
}
