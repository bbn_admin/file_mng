<?php


namespace app\common;


class ErrorCode
{
    const CODE_SUCC = "0";//成功代码
    const CODE_DB_ERROR = "9005";//数据库写入失败
    const CODE_RECORD_NOT_FOUND = "9404";//记录未找到或已被删除0
    const CODE_TOKEN_ERR = "12";
    const CODE_TOKEN_EXPIRE = "11";
    const CODE_TOKEN_FORMAT_ERR = "14";
    const CODE_TOKEN_NONE = "13";

    /**
     * 返回错误代内容
     * @param $code
     * @return mixed
     */
    public static function getError($code)
    {
        $errArr = self::getErrorArr();
        if (!key_exists($code, $errArr)) {
            return "未知错误";
        }
        return $errArr[$code];
    }

    /**
     * 获取错误码数组
     * @return array
     */
    protected static function getErrorArr()
    {
        return [
            self::CODE_SUCC => "成功",
            //100以内，需要重新登录
            self::CODE_TOKEN_EXPIRE => "token过期",
            self::CODE_TOKEN_ERR => "token不正确或已失效",
            self::CODE_TOKEN_NONE => "缺少token",
            self::CODE_TOKEN_FORMAT_ERR => "token格式不正确",
            "9001" => "缺少签名",
            "9002" => "签名不正确",
            "9004" => "请求已过期",
            self::CODE_DB_ERROR => "数据写入失败，请稍后再试",
            self::CODE_RECORD_NOT_FOUND => "记录未找到或已被删除",
            "9999" => "系统错误",
        ];
    }
}