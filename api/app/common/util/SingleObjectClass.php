<?php
/**
 * Created by PhpStorm.
 * User: yckj_lzj
 * Date: 2019-08-21
 * Time: 16:38
 */

namespace app\common\util;


use think\Log;

class SingleObjectClass
{
    protected $openLog=true;
    //输出结果
    protected function writeln($info, $type = "debug")
    {
        if ($this->openLog) {
            Log::write(print_r($info, true), $type);
        }
    }

    /**
     * 返回内容
     * @param $code
     * @param $data
     */
    protected function getResult($code, $data = null)
    {
        $result['code'] = $code;
        $result['msg'] = ActQuestionErrCode::$errInfo[$code];
        if ($data) {
            $result['data'] = $data;
        }
        return $result;
    }

    /**
     * 类实例化（单例模式）
     */
    public static function instance()
    {
        static $_instance = array();

        $classFullName = get_called_class();
        if (!isset($_instance[$classFullName])) {
            // $_instance[$classFullName] = new $classFullName();
            // 1、先前这样写的话，PhpStrom 代码提示功能失效；
            // 2、并且中间变量不能是 数组，如 不能用 return $_instance[$classFullName] 形式返回实例对象，否则 PhpStrom 代码提示功能失效；
            $instance = $_instance[$classFullName] = new static();
            return $instance;
        }

        return $_instance[$classFullName];
    }

}