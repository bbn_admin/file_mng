<?php
/**
 * Created by PhpStorm.
 * User: yckj-yf7
 * Date: 2021/9/24
 * Time: 17:25
 */

namespace app\common\util;

/**
 * 工具类
 * @package app\common\util
 */
class Util
{
    /**
     * 获取一定配置下的父子级的数据集合
     * @param $list 一维的数据集合
     * @param array $option 配置
     *              field 判断子级的字段名，默认为id
     *              parent_field 判断父级的字段名，默认为pid
     *              children 写入返回值的字段名，默认为children
     *              default 默认搜索开始的数值，默认为0
     * @return array|mixed
     */
    public static function getChildrenList($list, $option = []){
        $field = isset($option['field'])?$option['field']:'id';
        $parent_field = isset($option['parent_field'])?$option['parent_field']:'pid';
        $children_name = isset($option['children'])?$option['children']:'children';
        $default = isset($option['default'])?$option['default']:0;

        //按父级生成索引数组
        $index_data = [];
        foreach ($list as $item){
            $index_data[$item[$parent_field]][] = $item;
        }
        unset($item);

        return self::setChildren($index_data,$default,$field,$children_name);
    }

    /**
     * 写入子集
     * @param $index_data 索引数组
     * @param $pid 父级数值
     * @param $field 关联字段名
     * @param $children_name 写入的子集字段名
     * @return array|mixed
     */
    private static function setChildren($index_data, $pid, $field, $children_name){
        if (isset($index_data[$pid])){
            $list = $index_data[$pid];
            foreach ($list as &$item){
                $children = self::setChildren($index_data,$item[$field],$field, $children_name);
                if (count($children)>0){
                    $item[$children_name] = $children;
                }
            }
            return $list;
        }else{
            return [];
        }
    }

    /**
     * 获取两个日期之间的日期数组
     * @param $start_time
     * @param $end_time
     * @return mixed
     */
    public static function getPeriodDate($start_time,$end_time){
        $start_time = strtotime($start_time);
        $end_time = strtotime($end_time);
        $i = 0;
        $arr = [];
        while ($start_time <= $end_time){
            $arr[$i] = date('Y-m-d',$start_time);
            $start_time = strtotime('+1 day',$start_time);
            $i++;
        }

        return $arr;
    }

    /**
     * 根据数字转换成excel的字母
     * @param $num
     * @return string
     */
    public static function numToExcelLetter($num)
    {
        //由于大写字母只有26个，所以基数为26
        $base = 26;
        $result = '';
        while ($num > 0 ) {
            $mod = (int)($num % $base);
            $num = (int)($num / $base);

            if($mod == 0){
                $num -= 1;
                $temp = self::numToLetter($base) . $result;
            } elseif ($num == 0) {
                $temp = self::numToLetter($mod) . $result;
            } else {
                $temp = self::numToLetter($mod) . $result;
            }
            $result = $temp;
        }

        return $result;
    }

    /**
     * 数字转字母
     * @param $num
     * @return string
     */
    public static function numToLetter($num)
    {
        if ($num == 0) {
            return '';
        }

        $num = (int)$num - 1;
        //获取A的ascii码
        $ordA = ord('A');
        return chr($ordA + $num);
    }
}
