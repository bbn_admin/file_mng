<?php
/**
 * Created by PhpStorm.
 * User: yckj_lzj
 * Date: 2019-10-22
 * Time: 19:27
 */

namespace app\common\util;


class Rsa extends SingleObjectClass
{
    protected $priKeyStr;
    protected $pubKeyStr;

    public  function setPriPubKey($pubKey,$priKey){
        $this->priKeyStr=$priKey;
        $this->pubKeyStr=$pubKey;
//        $this->$pubKeyStr=$pubKey;
    }

    public function setPriKey($priKey){
        $this->priKeyStr=$priKey;
    }

    public function setPubKey($pubKey){
        $this->pubKeyStr=$pubKey;
    }

    private  function getPriKeyStr()
    {
        return "-----BEGIN PRIVATE KEY-----\n".$this->priKeyStr."\n-----END PRIVATE KEY-----";
    }

    public  function getPubKeyStr(){
        return "-----BEGIN PUBLIC KEY-----\n".$this->pubKeyStr."\n-----END PUBLIC KEY-----";
    }


    /**
     * 获取公钥     *
     * @return bool|resource
     */
    private  function getPublicKey()
    {


        $content = $this->getPubKeyStr();

        return openssl_pkey_get_public($content);
    }

    /**
     * 获取私钥
     * @return bool|resource
     */
    private  function getPrivateKey()
    {

        $content = $this->getPriKeyStr();
//        echo $content;
        return openssl_pkey_get_private($content);
    }


    /**
     * 私钥加密
     * @param $signString
     * @return string
     */
    public  function priSign($signString){
        $privKeyId = $this->getPrivateKey();
        $signature = '';
        openssl_sign($signString, $signature, $privKeyId);
        openssl_free_key($privKeyId);
        return base64_encode($signature);

    }


    /**
     * 私钥加密
     * @param string $data
     * @return null|string
     */
    public  function privEncrypt($data = '')
    {

        if (!is_string($data)) {
            return null;
        }
        return openssl_private_encrypt($data, $encrypted, $this->getPrivateKey()) ? base64_encode($encrypted) : null;
    }

    /**
     * 公钥加密
     * @param string $data
     * @return null|string
     */
    public  function publicEncrypt($data = '')
    {
        if (!is_string($data)) {
            return null;
        }
        var_dump($this->getPublicKey());
        return openssl_public_encrypt($data, $encrypted, $this->getPublicKey()) ? base64_encode($encrypted) : null;
    }

    /**
     * 私钥解密
     * @param string $encrypted
     * @return null
     */
    public  function privDecrypt($encrypted = '')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_private_decrypt(base64_decode($encrypted), $decrypted, $this->getPrivateKey())) ? $decrypted : null;
    }

    /**
     * 公钥解密
     * @param string $encrypted
     * @return null
     */
    public  function publicDecrypt($encrypted = '')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_public_decrypt(base64_decode($encrypted), $decrypted, $this->getPublicKey())) ? $decrypted : null;
    }
}