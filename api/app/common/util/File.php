<?php


namespace app\common\util;


class File extends SingleObjectClass
{
    /**
     * 文件大小单位自动转换函数
     * @param $size
     * @return string
     */
    public function getRealSize($size)
    {
        $kb = 1024;   // Kilobyte
        $mb = 1024 * $kb; // Megabyte
        $gb = 1024 * $mb; // Gigabyte
        $tb = 1024 * $gb; // Terabyte
        if ($size < $kb) {
            return $size . " B";
        } else if ($size < $mb) {
            return round($size / $kb, 2) . " KB";
        } else if ($size < $gb) {
            return round($size / $mb, 2) . " MB";
        } else if ($size < $tb) {
            return round($size / $gb, 2) . " GB";
        } else {
            return round($size / $tb, 2) . " TB";
        }
    }

    /**
     * 获取目录列表
     * @param $path
     * @return array
     */
    public function getDirContent($path)
    {
        if (!is_dir($path)) {
            return [];
        }
        //scandir方法
        $arr = array();
        $data = scandir($path);//扫描当前目录下的所有文件
        $floderArr = [];//文件夹数组
        $fileArr = [];//文件数组
        foreach ($data as $value) {
            if ($value == ".") {
                continue;
            }
            $filePath = $path . $value;
            $item = [];
            $item["name"] = $value;
            $isDir = is_dir($filePath);
            $item["type"] = $isDir ? "floder" : "file";//判断文件是不是文件，true是目录，false是文件

            //计算文件或文件夹大小
            if ($isDir) {
                $item["size"] = 0;//getRealSize( getDirSize($path.$value));
            } else {
                $item["size"] = $this->getRealSize(filesize($filePath));
            }
            $item["time_create"] = date("m-d H:i", filemtime($filePath));
            $item["time_active"] = date("m-d H:i", fileatime($filePath));
            $item["time_edit"] = date("m-d H:i", filectime($filePath));
            if ($isDir) {
                $floderArr[] = $item;
            } else {
                $fileArr[] = $item;
            }

        }
        $arr = array_merge($floderArr, $fileArr);
        return $arr;
    }

    /**
     * 获取文件夹大小，文件夹文件多比较耗时
     * @param $dir
     * @return false|int
     */
    public function getDirSize($dir)
    {
        $sizeResult = 0;
        $handle = opendir($dir);
        while (false !== ($FolderOrFile = readdir($handle))) {
            if ($FolderOrFile != "." && $FolderOrFile != "..") {
                if (is_dir("$dir/$FolderOrFile")) {
                    $sizeResult += getDirSize("$dir/$FolderOrFile");
                } else {
                    $sizeResult += filesize("$dir/$FolderOrFile");
                }
            }
        }
        closedir($handle);
        return $sizeResult;
    }

    /**
     * 删除文件
     * @param $path 绝对路径
     */
    public function delFile($path)
    {
        $url = iconv('utf-8', 'gbk', $path);
        if (PATH_SEPARATOR == ':') { //linux
            unlink($path);
        } else {  //Windows
            unlink($url);
        }
    }

    /**
     * 删除目录
     * @param $path 绝对路径
     */
    public function deleteDir($path)
    {
        $openpath = opendir($path);
        while ($f = readdir($openpath)) {
            $file = $path . '/' . $f;
            if ($f != '.' && $f != '..') {
                if (is_dir($file)) {
                    $this->deletedir($file);
                } else {
                    unlink($file);
                }
            }
        }
        closedir($openpath);
        rmdir($path);
    }

}