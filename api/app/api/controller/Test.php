<?php


namespace app\api\controller;


class Test extends Base
{
    protected $checkApiSignOpen = false;

    public function test1()
    {
        echo "this is test1\n";
//        var_dump(dirname("/api/public/"));
//        var_dump(root_path());
//        var_dump(public_path());
//        var_dump(dirname("./"));
        $path=root_path()."//public/../../../../../../../";//dirname(__FILE__."/../");
        //文件路径正则   ^.{0,2}\/?(\S*\/?)+$
        var_dump($path);
        print_r($this->getDirContent($path));
    }

    /**
     * 获取目录文件信息
     * @param $path
     * @return array|false
     */
    protected function getDirContent($path)
    {
        if (!is_dir($path)) {
            return [];
        }
        //readdir方法
        /* $dir = opendir($path);
        $arr = array();
        while($content = readdir($dir)){
          if($content != '.' && $content != '..'){
            $arr[] = $content;
          }
        }
        closedir($dir); */

        //scandir方法
        $arr = array();
        $data = scandir($path);//扫描当前目录下的所有文件
        foreach ($data as $value) {
            $item = [];
            $item["name"] = $value;
            $isDir= is_dir($path . $value);
            $item["type"] =$isDir ? "floder" : "file";//判断文件是不是文件，true是目录，false是文件
            //计算文件或文件夹大小
            if($isDir){
                $item["size"]=0;//getRealSize( getDirSize($path.$value));
            }else{
                $item["size"]=getRealSize(filesize($path.$value));
            }

            $arr[] = $item;
        }
        return $arr;
    }
}