<?php


namespace app\api\controller;


use think\facade\Filesystem;

class File extends Base
{
    protected $checkApiSignOpen = false;//是否启用签名

    /**
     * 获取文件夹列表
     */
    public function getFloderList(){
        $url=input("path","/");
        $path=public_path().$url."";//dirname(__FILE__."/../");
        //文件路径正则   ^.{0,2}\/?(\S*\/?)+$
        $list=\app\common\util\File::instance()->getDirContent($path);
        $this->success($list);

    }

    /**
     * 删除文件
     */
    public function delFile(){
        $url=input("path","/");
        $path=public_path().$url."";//dirname(__FILE__."/../");
        $isDir = is_dir($path);

        if($isDir){
            //如果是文件夹
            \app\common\util\File::instance()->deleteDir($path);
        }else{
            \app\common\util\File::instance()->delFile($path);
        }
        $this->success("");
    }

}