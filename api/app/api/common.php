<?php
if (!function_exists('arrayToDic')) {
    /**
     * 数组转键值对数组
     * @param $array
     */
    function arrayToDic($array,$keyName="value",$valueName="label"){
        $dic=[];
        foreach ($array as $key=>$value){
            $item[$keyName]=$key;
            $item[$valueName]=$value;
            $dic[]=$item;
        }
        return $dic;
    }

}

