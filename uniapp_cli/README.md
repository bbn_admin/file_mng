### 使用说明

#### 工具使用

- 使用 hbuilder
  使用 hbuilderx，只需要加载 src 目录运行即可。
- 使用命令行 cli
  - 使用 npm
    先使用`npm i` 安装依赖，然后使用 `npm fun dev:%platform-name%` 运行
  - 使用 pnpm
    pnpm 比 npm 运行更快，推荐使用。
    注意： 由于 pnpm 在生成小程序有 bug， 如果开发微信小程序且有使用 easycom 引入插件，如使用 uview-ui，则不要使用 pnpm 安装

#### 使用 hubilder 和命令行 cli 差别：

- cli 创建的项目，编译器安装在项目下。并且不会跟随 HBuilderX 升级。如需升级编译器，可以使用 @dcloudio/uvm 管理编译器的版本，如 npx @dcloudio/uvm。
- HBuilderX 可视化界面创建的项目，编译器在 HBuilderX 的安装目录下的 plugin 目录，随着 HBuilderX 的升级会自动升级编译器。
- 已经使用 cli 创建的项目，如果想继续在 HBuilderX 里使用，可以把工程拖到 HBuilderX 中。注意如果是把整个项目拖入 HBuilderX，则编译时走的是项目下的编译器。如果是把 src 目录拖入到 HBuilderX 中，则走的是 HBuilderX 安装目录下 plugin 目录下的编译器。
- cli 版如果想安装 less、scss、ts 等编译器，需自己手动 npm 安装。在 HBuilderX 的插件管理界面安装无效，那个只作用于 HBuilderX 创建的项目。(注：本项目已经引入了 sass 和 less 的依赖)

参考：https://uniapp.dcloud.io/quickstart-cli

更新：2021-12-25
