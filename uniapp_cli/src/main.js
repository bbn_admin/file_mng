import Vue from 'vue'
import App from './App'


//引入版本组件
import version from "components/yc/version.vue";
Vue.component('version', version);

import util from 'libs/util/util';
Vue.prototype._util = util;
Vue.config.productionTip = false

App.mpType = 'app'



const app = new Vue({
  ...App
})
app.$mount()
