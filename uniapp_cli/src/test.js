
function delay(milliseconds) {
    return new Promise((resolve, reject) => {
        setTimeout(function (e) {
            e = true;
            resolve(e);
            console.log("delay finished");
        }, milliseconds);
    });
}

function a() {

}

async test1(){
    delay(1000);
    a();
    delay(2000);
    a();
    delay(3000);
    a();
}
