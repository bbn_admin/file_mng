const config = {
	domain: "http://192.168.2.80:38082/", //请求域名
	host: "", //接口请求地址
	api_sign_key: "37fba926fc83be1cf4a7e3ee09110bef",//接口签名参数
};

if (process.env.NODE_ENV === 'development') {
	//开发环境配置
	config.domain = "http://local.lzj/file_mng/api/public/";
}
config.host = config.domain + "index.php";//接口地址
export default config;
