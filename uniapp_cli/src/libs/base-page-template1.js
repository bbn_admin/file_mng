import util from "./util/util";

const mixin = {
	data() {
		return {
			title: 'Hellomix'
		}
	},
	async onLoad() {
		console.log("basePage onLoad", this.title);
		this.test("base");
	},
	async onPullDownRefresh() {
		console.log("basePage onPullDownRefresh");
		await util.delay(1000);
		uni.stopPullDownRefresh();
	},
	methods: {
		test(from = "base") {
			console.log("base test " + from);
		},
		test1() {
			console.log("basePage test");

		},
		/**
		 * 加载数据，所有页面按这个命名
		 */
		loadData() {

		},
		//=============日志方法==============
		/**
		 * 警告日志
		 */
		logW() {
			console.warn("warn", ...arguments);
		},
		/**
		 * 错误日志
		 */
		logE() {
			console.error("error", ...arguments);
		},
		/**
		 * 普通日志
		 */
		log() {
			console.log("log", ...arguments)
		},
		/**
		 * 信息日志
		 */
		logI() {
			console.info("info", ...arguments);
		},
		/**
		 * 调试日志
		 */
		logD() {
			console.debug("debug", ...arguments);
		}
		//===========日志方法=============
	}
}
export default mixin;
