import util from "./util/util";
const tagInfo = "[/libs/base-page.js]:";
const mixin = {
	data() {
		return {
			title: 'Hellomix',
			adminId: "",
			themeColor: "#41c872",
		}
	},
	async onLoad() {
		console.log("basePage onLoad begin");
		if (this.$options.name) {
			let tempData = util.storage.get(this.$options.name);
			if (tempData) {
				// this.$data = tempData;
				Object.assign(this.$data, tempData)
			}
		}

		//console.log("basePage onLoad end");
	},
	async onPullDownRefresh() {
		//console.log("basePage onPullDownRefresh");
		this.logI(tagInfo, "basePage onPullDownRefresh");
		await this.loadData();
		uni.stopPullDownRefresh();
	},
	methods: {
		//========获取数据=============

		//=========获取数据===========
		//========功能方法===========
		/**
		 * 检测是否登录 
		 */
		checkLogin() {
			let token = this._util.storage.get("token");
			this.logI(tagInfo, "checkLogin", token, "**", this._util.storage.get("token"), "**");
			if (!token) {

				uni.reLaunch({
					url: "/pages/index/index"
				})
			}

		},
		setToken(token) {
			this._util.storage.set("token", token);
		},
		removeToken() {
			this._util.storage.remove("token");
		},
		/**
		 * 加载数据，所有页面按这个命名
		 */
		loadData() {
			this.logI(tagInfo, "base loadData");
		},
		initSaveData() {
			//console.log("initSaveData", this.$data, this, this.$options.name);
			if (this.$options.name) {
				if (this.$options.name.indexOf("cache-") > -1) {
					//先暂时屏蔽掉缓存
					//util.storage.set(this.$options.name, this.$data);
				}
			}


		},
		initLoadData() {

		},

		navigateBack(delta = 1) {
			uni.navigateBack({
				delta: delta
			});
		},
		navigateTo(url) {
			this.logI(tagInfo, "navigateTo", url);
			uni.navigateTo({
				url: url
			});
		},
		//=========功能方法结束============
		//=============日志方法==============
		/**
		 * 警告日志
		 */
		logW() {
			console.warn("warn", ...arguments);
		},
		/**
		 * 错误日志
		 */
		logE() {
			console.error("error", ...arguments);
		},
		/**
		 * 普通日志
		 */
		log() {
			console.log("log", ...arguments)
		},
		/**
		 * 信息日志
		 */
		logI() {
			console.info("info", ...arguments);
		},
		/**
		 * 调试日志
		 */
		logD() {
			console.debug("debug", ...arguments);
		},
		//===========日志方法=============
		//==========测试方法================

		//==========测试方法结束==============
	}
}
export default mixin;
