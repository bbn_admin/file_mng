const tagInfo = '[/libs/model/File.js]：';
import http from '../net/request';
import apiUrls from "../net/urls";

const File = {

    /**
     * 用户登录 
     * @param {*} login_name 
     * @param {*} password 
     * @returns 
     */
    async getFloderList(path) {
        let url = apiUrls.getFloderList;
        let param = {
            path
        };
        let res = await http.get(url, param);
        if (res.code == 0) {
            res.data.forEach(item => {
                item.scroll_left = 0;
                item.scroll_left_temp = 0;
            });
        }
        return res;
    },

    async delFile(path) {
        let url = apiUrls.delFile;
        let param = {
            path
        };
        let res = await http.get(url, param);
        return res;
    }


}
export default File;