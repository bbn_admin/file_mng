import stringHelper from "./util.string";
import storage from "./util.storage";
import message from "./util.message";
import check from "./util.check";
const util = {
	string: stringHelper,
	delay: function (milliseconds) {
		return new Promise((resolve, reject) => {
			setTimeout(function (e) {
				e = true;
				resolve(e);
				console.log("delay finished");
			}, milliseconds);
		});
	},
	message,
	storage,
	check,
}
export default util
