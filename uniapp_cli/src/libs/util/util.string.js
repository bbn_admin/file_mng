/**
 * 将对象每个元素都转成字符串
 */
function arrayStringVal(data) {
  for (const index in data) {
    data[index] = String(data[index]);
  }
  return data;
}

function parseString(obj) {
  if (!obj && typeof obj !== 'undefined' && obj != 0) {
    return '';
  }
  if (typeof obj === 'undefined') {
    return '';
  }
  if (typeof obj === 'number' && isNaN(obj)) {
    return '0';
  }
  return obj.toString();
}

function showPeriodNameById(periodId) {
  const year = parseInt(periodId / 100);
  let month = parseInt(periodId % 100);
  month = month > 9 ? month : '0'.concat(month);
  return year + '年第' + month + '期';
}

const stringHelper = {
  arrayStringVal,
  parseString,
  showPeriodNameById
};

export default stringHelper;
