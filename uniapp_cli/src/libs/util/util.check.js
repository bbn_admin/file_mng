const check = {
    phone(str) {
        if (!(/^1(3|4|5|6|7|8|9)\d{9}$/.test(str))) {

            return false;
        }
        return true;
    }
};
export default check;