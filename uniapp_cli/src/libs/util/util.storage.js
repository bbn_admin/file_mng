const storage = {
    set(key, data) {
        uni.setStorageSync(key, data);
    },
    get(key) {
        return uni.getStorageSync(key);
    },
    remove(key) {
        return uni.removeStorageSync(key);
    },
    clear() {
        return uni.clearStorageSync();
    }
};
export default storage;