const message = {
    /**
     * 加载中
     * @param {*} title 
     */
    loading(title = "加载中...") {
        uni.showLoading({
            title: title,
            mask: true,
        });
    },
    /**
     *  隐藏 loading 提示框
     */
    hideLoading() {
        uni.hideLoading();
    },
    /**
     * 消息提示，有确认按钮
     * @param {*} msg 
     */
    alert(msg) {
        uni.showModal({
            content: msg,
            showCancel: false
        });
    },
    /**
     * 消息二次确认
     * @param {*} title 
     * @param {*} content 
     * @returns 
     */
    async confirm(title, content) {

        return new Promise((resolve, reject) => {
            uni.showModal({
                title: title,
                content: content,
                success: function (res) {
                    if (res.confirm) {
                        console.log('用户点击确定');
                        resolve(true);
                    } else if (res.cancel) {
                        console.log('用户点击取消');
                        resolve(false);
                    }
                }
            });
        });
    }
};
export default message;