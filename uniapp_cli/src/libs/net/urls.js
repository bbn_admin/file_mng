const url = {
    getFloderList: "/api/file/getFloderList",//获取文件夹列表
    delFile: "/api/file/delFile",//删除文件或文件夹
};
export default url;