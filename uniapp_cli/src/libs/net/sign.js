import md5 from 'js-md5';
import config from "@/config.js";
import util from "../util/util.js";
const api_sign_secret = config.api_sign_key;//签名密钥
const tagInfo = "/libs/net/sign.js";
export default function (data) {
  if (!data) {
    data = {};//如果没有传data就实例化一个空对象
  }
  const timestamp = parseInt((new Date()).getTime() / 1000);//获取当前时间戳（秒）
  data._timestamp = timestamp;
  const sortData = {};
  Object.keys(data).sort().forEach((key) => {
    sortData[key] = util.string.parseString(data[key]);//对data所有参数按键值进行排序 
  });
  //console.log(tagInfo, JSON.stringify(sortData) + api_sign_secret);
  sortData._sign = md5(JSON.stringify(sortData) + api_sign_secret);//签名=md5(参数的json字符串+签名密钥)
  return sortData;
};
